package Atelier2;

public class Main {
    public static void main(String[] args) {
        // Création des terrains
        Terrain rueDeLaPaix = new Terrain("Rue de la Paix", 400);
        Terrain rueDeCourcelles = new Terrain("Rue de Courcelles", 100);

        // Affichage des terrains
        rueDeLaPaix.afficher();
        rueDeCourcelles.afficher();

        // Création de la gare
        Gare gareMontparnasse = new Gare("Gare Montparnasse", 200);

        // Affichage de la gare
        gareMontparnasse.afficher();
    }
}
