package Atelier2;
public class FabriqueProduit {
    public Propriete creer(String type, String nom, int prix) {
        switch (type.toLowerCase()) {
            case "terrain":
                return new Terrain(nom, prix);
            case "gare":
                return new Gare(nom, prix);
            case "compagnieee":
                return new Compagnie(nom, prix);
            default:
                throw new IllegalArgumentException("Type de propriété non supporté");
        }
    }
}
