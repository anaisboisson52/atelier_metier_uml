package Atelier2;
public class Terrain extends Propriete {
    public Terrain(String nom, int prix) {
        super(nom, prix);
    }

    @Override
    public void afficher() {
        System.out.println("Terrain: " + getNom() + ", Prix: " + getPrix());
    }
}
