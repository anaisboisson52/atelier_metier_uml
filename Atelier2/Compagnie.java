package Atelier2;
public class Compagnie extends Propriete {
    public Compagnie(String nom, int prix) {
        super(nom, prix);
    }

    @Override
    public void afficher() {
        System.out.println("Compagnie: " + getNom() + ", Prix: " + getPrix());
    }
}
