package Atelier2;
public class Gare extends Propriete {
    public Gare(String nom, int prix) {
        super(nom, prix);
    }

    @Override
    public void afficher() {
        System.out.println("Gare: " + getNom() + ", Prix: " + getPrix());
    }
}
