package Atelier5;

public class MVCMain {
    public static void main(String[] args) {
        // 1- récupération d’un joueur de la base de données (simulé)
        Joueur joueur = getJoueurDB();

        // 2- création d’une vue Joueur
        JoueurVue vue = new JoueurVue();

        // 3- création d’un controleur Joueur et affichage (de la vue)
        JoueurControleur controleur = new JoueurControleur(joueur, vue);
        controleur.updateVue();

        // 4- ajout de 100€ au cash du joueur
        double nouveauCash = joueur.getCash() + 100;
        joueur.setCash(nouveauCash);

        // 5- mise à jour de la vue
        controleur.updateVue();
    }

    // Méthode simulée pour récupérer un joueur de la base de données
    public static Joueur getJoueurDB() {
        return new Joueur("Alice", 500.0);
    }
}
