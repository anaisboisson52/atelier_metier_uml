package Atelier5;

public class JoueurControleur {
    private Joueur modele;
    private JoueurVue vue;

    public JoueurControleur(Joueur modele, JoueurVue vue) {
        this.modele = modele;
        this.vue = vue;
    }

    public String getPrenom() {
        return modele.getPrenom();
    }

    public void setPrenom(String prenom) {
        modele.setPrenom(prenom);
    }

    public double getCash() {
        return modele.getCash();
    }

    public void setCash(double cash) {
        modele.setCash(cash);
    }

    public void updateVue() {
        vue.afficherFicheJoueur(modele.getPrenom(), modele.getCash());
    }
}
