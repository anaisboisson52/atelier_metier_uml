package Atelier4;
public class Joueur {
    private String prenom;
    private double cash;

    public Joueur(String prenom, double cash) {
        this.prenom = prenom;
        this.cash = cash;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "prenom='" + prenom + '\'' +
                ", cash=" + cash +
                '}';
    }
}
