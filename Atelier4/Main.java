package Atelier4;
import java.util.List;
public class Main {
    public static void main(String[] args) {
        JoueurDAO joueurDAO = new JoueurDaoImpl();

        // 1- Récupération de la liste des joueurs
        List<Joueur> joueurs = joueurDAO.getTousLesJoueurs();
        System.out.println("Liste initiale des joueurs:");
        for (Joueur joueur : joueurs) {
            System.out.println(joueur);
        }

        System.out.println();

        // 2- Ajout de 100€ à chaque joueur
        System.out.println("Liste des joueurs mise a jour:");
        for (Joueur joueur : joueurs) {
            joueur.setCash(joueur.getCash() + 100.0);
            System.out.println(joueur);
        }

        System.out.println();
        

        // 3- Mise à jour des joueurs
        for (Joueur joueur : joueurs) {
            joueurDAO.updateJoueur(joueur);
        }

        // 4- Suppression d’un joueur
        joueurDAO.deleteJoueur("Bob");

        // 5- Récupération de la liste des joueurs pour affichage
        joueurs = joueurDAO.getTousLesJoueurs();
        System.out.println("Liste des joueurs après la suppression d'un joueur:");
        for (Joueur joueur : joueurs) {
            System.out.println(joueur);
        }
    }
}
  