package Atelier4;
import java.util.List;

public interface JoueurDAO {
    List<Joueur> getTousLesJoueurs();
    void addJoueur(Joueur joueur);
    void updateJoueur(Joueur joueur);
    void deleteJoueur(String prenom);
}
