package Atelier4;
import java.util.ArrayList;
import java.util.List;

public class JoueurDaoImpl implements JoueurDAO {
    private List<Joueur> joueurs;

    public JoueurDaoImpl() {
        joueurs = new ArrayList<>();
        // Ajout de 3 joueurs préenregistrés
        joueurs.add(new Joueur("Alice", 500.0));
        joueurs.add(new Joueur("Bob", 300.0));
        joueurs.add(new Joueur("Charlie", 250.0));
    }

    @Override
    public List<Joueur> getTousLesJoueurs() {
        return joueurs;
    }

    @Override
    public void addJoueur(Joueur joueur) {
        joueurs.add(joueur);
    }

    @Override
    public void updateJoueur(Joueur joueur) {
        for (int i = 0; i < joueurs.size(); i++) {
            if (joueurs.get(i).getPrenom().equals(joueur.getPrenom())) {
                joueurs.set(i, joueur);
                return;
            }
        }
    }

    @Override
    public void deleteJoueur(String prenom) {
        joueurs.removeIf(joueur -> joueur.getPrenom().equals(prenom));
    }
}
