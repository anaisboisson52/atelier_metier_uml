package Atelier1;
public class Main {
    public static void main(String[] args) {
         // Création d'un premier objet b1 de type Banque, cash = 1000
        Banque b1 = Banque.getInstance();
        b1.setCash(1000);
        
        // Affichage du cash de b1
        System.out.println("Cash de b1: " + b1.getCash());

        // Création d'un second objet b2 de type Banque, cash = 500
        Banque b2 = Banque.getInstance();
        b2.setCash(500);

        // Affichage du cash de b2
        System.out.println("Cash de b2: " + b2.getCash());

        // Affichage du cash de b1
        System.out.println("Cash de b1: " + b1.getCash());
    }
}
