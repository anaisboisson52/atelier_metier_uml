package Atelier1;
public class Banque {
    private static Banque instance;

    private int cash;

    private Banque(int cash) {
        this.cash = cash;
    }

    // Méthode publique statique pour obtenir l'instance unique
    public static Banque getInstance() {
        if (instance == null) {
            // Création de l'instance si elle n'existe pas déjà
            instance = new Banque(1000); // Exemple avec 1000 comme valeur initiale de cash
        }
        return instance;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }
}

