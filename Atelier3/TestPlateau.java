package Atelier3;
public class TestPlateau {
    public static void main(String[] args) {
        //Objet Plateau
        Plateau plateau = new Plateau();

        //Création des 10 premières cases du jeu et ajout dans le plateau
        for (int i = 1; i <= 10; i++) {
            Case c = new Case(i, "Case " + i);
            plateau.ajouterCase(c);
        }

        //Boucle parcours du plateau, affichage des cases.
        for (Case c : plateau) {
            c.afficher();
        }
    }
}
