package Atelier3;
import java.util.ArrayList;
import java.util.Iterator;

public class Plateau implements Iterable<Case> {
    private ArrayList<Case> cases;

    public Plateau() {
        cases = new ArrayList<>();
    }

    public void ajouterCase(Case c) {
        cases.add(c);
    }

    public Case getCase(int i) {
        if (i >= 0 && i < cases.size()) {
            return cases.get(i);
        }
        return null;
    }

    public int nbCases() {
        return cases.size();
    }

    @Override
    public Iterator<Case> iterator() {
        return new PlateauIterator(this);
    }
}

class PlateauIterator implements Iterator<Case> {
    private Plateau plateau;
    private int currentIndex = 0;

    public PlateauIterator(Plateau plateau) {
        this.plateau = plateau;
    }

    @Override
    public boolean hasNext() {
        return currentIndex < plateau.nbCases();
    }

    @Override
    public Case next() {
        if (!hasNext()) {
            throw new java.util.NoSuchElementException();
        }
        return plateau.getCase(currentIndex++);
    }
}
